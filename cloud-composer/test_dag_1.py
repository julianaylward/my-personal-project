#Import libraries
import requests
import pandas as pd
import io
import numpy as np
#from sqlalchemy import create_engine
#from dotenv import load_dotenv, find_dotenv
import os
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago

dag = DAG(
    "test_dag_1",
    default_args={
        "owner": "Julian",
        "start_date": days_ago(1),
    },
    schedule_interval='0 */12 * * *',  #every 6 hours
    dagrun_timeout=timedelta(minutes=5)
)

# This actually works!
def py_job(ds, **kwargs):
    today = datetime.today()
    rand = np.random.rand(3,2)
    df = pd.DataFrame(rand,index=[today]*3)
    df.to_csv("data/test_dag_data.csv")
    print(df.head())
    




    #Fetch stores and pass to Adobe SFTP
create_random_data = PythonOperator(
    task_id='create_random_data',
    provide_context=True,
    python_callable=py_job,
    #params={"url_str": "https://majestorage.blob.core.windows.net/majestorage%5CContent/Images/Uploaded/Xml%5Cxmlfeed.xml"},
    dag=dag,
)

py_job("ds")
