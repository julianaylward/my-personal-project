#https://matplotlib.org/devdocs/gallery/user_interfaces/web_application_server_sgskip.html
#Following matplotlibs examaple above:

import base64
from io import BytesIO

from flask import Flask
from flask import render_template
#from matplotlib.figure import Figure
import matplotlib.pyplot as plt 
import numpy as np

app = Flask(__name__)

# Fetch some data, ideally it would be something worth showing!
def get_data(multipler):
    values = [8,5,12,6]
    values2 = []
    for i in values:
        #print(i)
        i = i * multipler
        #print(i)
        values2.append(i)
        #print(values2)
    return values2


def create_bar_chart(values,var):
    #https://matplotlib.org/stable/gallery/lines_bars_and_markers/barchart.html#sphx-glr-gallery-lines-bars-and-markers-barchart-py
    labels = ["Cheese","Bread","Eggs","Sausages"]
    vals = values
    x = np.arange(len(labels))
    width = 0.35
    fig, ax = plt.subplots()
    rects = ax.bar(x, vals, width, label="Pack Size",align = "center")
    ax.set_ylabel("units")
    ax.set_xlabel("Item Name")
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()
    #ax.bar_label(rects, padding=3)
    fig.tight_layout()
    # Save it to a temporary buffer.
    buf = BytesIO()
    #save to buffer
    fig.savefig(buf, format="png")
    #save to file
    filename = f"static/chart_{var}.png"
    fig.savefig(filename)
    # Embed the result in the html output.
    #data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return filename

#Route decorator creates a single url equal to the function name
@app.route("/")
def home():
    #Fetch some data
    var = 2
    values = get_data(var)
    #Create and save the chart as a static png:
    filename = create_bar_chart(values,var)
    var = 1
    values = get_data(var)
    filename2 = create_bar_chart(values,var)
    #return f"<h1> A Really nice Chart </h1> <br> <a href='http://127.0.0.1:5000/hello-world'> click here to see more </a> <br> <br> <img src='data:image/png;base64,{data}'/>"
    return render_template("home.html",name="Julian",title = None)    #,data=data)

#A second route decorator holds another url and function
@app.route("/hello-world")
def hello2(name=None):
    return render_template("about.html",name="Kasia", title = "Work in progress")

#About page
@app.route("/about")
def about(name=None):
    return render_template("about.html",name="Julian", title = "About this site")

@app.route("/web-scraping")
def web_scraping():
    return render_template("web_scraping_members_of_parliament.html",title = "Web Scraping: Members of Parliament")
