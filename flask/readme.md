### Matplotllib with Flask

We can use a flask app to host outputs accessible to all via the web.

To inititiate, user:
- `export FLASK_APP=app`
- `FLASK_ENV=development`
- `flask run`
