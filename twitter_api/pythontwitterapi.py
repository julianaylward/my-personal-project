# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 10:13:19 2020
@author: jaylward
"""
#Start by setting up logging
import logging
logging.basicConfig(filename='tweets_logger.log',level=logging.INFO)

from datetime import datetime
#timestamp when the job starts running
timestampStr = datetime.now().strftime("%Y-%m-%d %H:%M:%S") 

#Log commence
logging.info('Starting script: ' + timestampStr)

import tweepy
import boto3	
import pandas as pd
import os
import re


#Following blog:
#https://www.toptal.com/python/twitter-data-mining-using-python

consumer_key= 'API KEY' #API KEY
consumer_secret='API SECRET KEY' #API SECRET KEY
access_token='ACCESS TOKEN' #ACCESS TOKEN
access_token_secret='ACCESS TOKEN SECRET' #ACCESS TOKEN SECRET

# Creating the authentication object
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
# Setting your access token and secret
auth.set_access_token(access_token, access_token_secret)
# Creating the API object while passing in auth information
api = tweepy.API(auth) 

##Import the pandas package

## Using pandas.read_csv import file
##Note you need to know your working directory! You can see this on the RHS in File explorer
##By Default, for me, its set to G:\Team Drives\EcommPC\Customer Analytics\Python 

twitter_ids = pd.read_csv('twitter_user_ids.csv')
twitter_ids_list = twitter_ids['twitter_username'].values.tolist()

#print(twitter_ids_list)

tweets = [] #initialise empty list
get_fields = ["tweet.id", "tweet.user.screen_name", "tweet.user.location", "tweet.user.description", 
        "tweet.user.friends_count", "tweet.user.followers_count", "tweet.user.listed_count", "tweet.user.favourites_count",
        "tweet.user.statuses_count", "tweet.user.created_at",
        "tweet.retweet_count", "tweet.favorite_count",
        "tweet.user.name", "tweet.text", "tweet.created_at", "len(tweet.text)", "event_datetime"]

#print(timestampStr)

#Loop through the Twitter users who we want to get tweets from
for username in twitter_ids_list:
    name = username #the row/element in the list, 1 element of nested list is twitter username
    # Number of tweets to pull
    tweetCount = 100
    # Calling the user_timeline function with our parameters
    results = api.user_timeline(id=name, count=tweetCount)

    # for each through all tweets pulled
    for tweet in results:
    # printing the text stored inside the tweet object
        #print (tweet.id, tweet.user.screen_name, tweet.user.location, tweet.user.description, 
        #tweet.user.friends_count, tweet.user.followers_count, tweet.user.listed_count, tweet.user.favourites_count,
        #tweet.user.statuses_count, tweet.user.created_at,
        #tweet.geo, tweet.retweet_count, tweet.favorite_count,
        #tweet.user.name, tweet.text, tweet.created_at, len(tweet.text), tweet)
        tmp = [tweet.id, tweet.user.screen_name, tweet.user.location, tweet.user.description, 
        tweet.user.friends_count, tweet.user.followers_count, tweet.user.listed_count, tweet.user.favourites_count,
        tweet.user.statuses_count, tweet.user.created_at,
        tweet.retweet_count, tweet.favorite_count,
        tweet.user.name, tweet.text, tweet.created_at, len(tweet.text), 
        timestampStr] #Add a timestamp to each record exported
        tweets.append(tmp) #Append for each output and 
        
#Commas are causing a headache in Athena, so jut remove them
#We're actually not that botherered about presenting the text so it doesn't matter

#We need to loop through what is a list of lists
#Not all fields are strings within the nested lists
#identify only the list values that are of data type = string

#Regex.
# This is necessary because non alphanumeric characters (emojis etc) as causing issues in Athen
# Since we don't really care about them either, just get rid of them!


tweets2 = [] #individual tweet (as a list)
tweets3 = [] # list of lists (tweets)

# NEED TO NEST ANOTHER LOOP
# tweets is a list of lists! so loop again!
for lists in tweets: #for each list (within the list of lists)
    for x in lists: #value within a list
        #print(type(x))
        if type(x) == str: #returns the data type (object) of each value in a list
            #tweets2.append(x.replace(',',';'))
            x.replace(',',';')
            x = re.sub('[^A-Za-z0-9 ;.:-]+',' ', x )
            #x = re.sub('\W+',' ',x)
            tweets2.append(x)
        else: 
            tweets2.append(x)
    tweets3.append(tweets2)
    tweets2 = [] # clear down tweets2 (individual tweet)

#Export data in csv format
tweets_df = pd.DataFrame(tweets3)
#Datestamp the file output
DateStr = datetime.now().strftime("%Y_%m_%d") 

#Write output to CSV
#Header can be skipped in Athena
#quoting = 2 = quote all non-numeric fields with quotation marks
#Commas are causing issues with columns being offset in Athena otherwise

#Specify filename and path to save 
filepath = 'tweets_' + DateStr + '.csv'
tweets_df.to_csv(filepath, header = get_fields, quoting = 2)


#Convert df to pyarrow table (for export as parquet)
#import pyarrow as pa
#tweets_table = pa.Table.from_pandas(tweets_df)


#Export data in Parquet format using pyarrow
#import pyarrow.parquet as pq

#pq.write_table(tweets_table, 'tweets_delta.parquet')

# ******************   UPLOAD DATA TO S3 ***********************
#Notes: # Generate credentials from IAM account (or Root but not recommended)
#Boto 3 is a package from Amazon to manage AWS
#boto 3 quickstart:https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html
#USE AMAZON COMMAND LINE INTERFACE TO CONFIGURE CREDENTIALS
# >> command promt >> amazon cli >> aws configure >> enter your credentials


###
## INSTEAD OF TRYING TO "INCREMENTALLY" ADD TO MY FACT_TWEETS TABLE IN ATHENA...
## Add incremental files and query over all of the file locations
## e.g. output to s3://juliantwitter/tweetevents/YYYY-MM-DD-HH-MM/
## Then in Athena you can query over the entire folder and select unique events (tweet ID and export timestamp)
## The export process can then run at any frequency in batch mode to near real time
###

# Let's use Amazon S3
s3 = boto3.resource('s3')

# Print out bucket names
#for bucket in s3.buckets.all():
#    print(bucket.name)

# Upload a new file
data = open(filepath, 'rb')
#data2 = open('tweets_delta.parquet','rb')
s3.Bucket('juliantwitter').put_object(Key= filepath, Body=data)
#s3.Bucket('juliantwitter').put_object(Key='tweets_delta.parquet',Body=data2)

#Close and delete the local file copy once transferred to S3
data.close()
os.remove(filepath)

#Log commence
logging.info('Finished script: ' + timestampStr)

#Close the logging session
logging.shutdown()


