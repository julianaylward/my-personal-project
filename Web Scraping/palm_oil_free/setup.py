from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='scrape, store and display palm oil free skus"',
    author='Julian Aylward',
    license='MIT',
)
