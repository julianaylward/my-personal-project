##Author: Julian Aylward. Date 04/02/2020
##Purpose, export DWH Table data for core tables to create documentation as part of handover
## For example, core table warehouse.d_orders, how many columns, what is the min and max value within each column, how many unique values does each column hold etc etc
## This can then be compared between original and Lego DWH

## Python connect to DW ##
##https://www.blendo.co/blog/access-your-data-in-amazon-redshift-and-postgresql-with-python-and-r/


import psycopg2
import pandas as pd

# 1: Extract a distinct list of data warehouse tables and columns
#Remove views sourced from data lake as cannot access

get_dw_tables = """select table_schema || '.'|| table_name as table_ref
                from pg_catalog.svv_tables
                where table_schema = 'warehouse'
                and table_name not like 'v_customer_subscription%'
                and table_name <> 'v_subscription_service_messages'
                order by table_name
            ;"""


get_dw_columns = """select table_schema, table_name, column_name, ordinal_position
            from pg_catalog.svv_columns
            where table_schema = 'warehouse'
            and table_name not like 'v_customer_subscription%'
            and table_name <> 'v_subscription_service_messages'
            order by table_name, ordinal_position
            ;""" 

sql_str = [get_dw_tables,get_dw_columns]

# Create empty outputs list
list_of_results = [] 

############# Fetch Data ###################

for sql in sql_str:
    #print(sql)
    con= psycopg2.connect(dbname='DBname', host='host',port='port',user='user',password='password')
    cur = con.cursor()
    cur.execute(sql) ##e.g. select some data. Triple quotation for multi line strings.
    tmp = cur.fetchall() ## Fetch the results into an arbitrary data object
    list_of_results.append(tmp) ##Add data into my list of outputs
    cur.close() ## Close cursor
    con.close() ## close connection

## NOW Split out data into two pandas data frames to work with

df_tables = pd.DataFrame(list_of_results[0])
df_columns = pd.DataFrame(list_of_results[1])
##select only the value from the series, will generate an integer for looping range
df_tables_count = df_tables.count()[0]  


#1) Create a list of lists with all column names in for each table



#find the number of columns within each table
df_columns_per_table = df_columns.groupby(1).agg({3: ['min', 'max']})
df_columns_per_table.columns = ['a', 'b'] #rename columns

# create a parent list, to append a series of lists within for columns of each table


columns_list_sum = ""   #empty string object
columns_list_min = ""   #empty string object
columns_list_max = ""   #empty string object
#columns_list_med = ""   #empty string object
#columns_list_stdev = ""   #empty string object
columns_list_count = ""   #empty string object
columns_list_countd = ""   #empty string object


# Arbitrary counter for looping
j = 0 #column name counter for df_columns
k = 0 #table name counter for df_columns_per_table
i = 0 #starting row index to find appropiate columns for the (next) table in loop sequence

my_sql_script_list = [] #load completed sql queries into a list

# Loop over each table name
for k in range(df_tables_count):   #for loops cannot iterate over a dataframe
    #print("loop start", k,j,i) #for loop is working
    while j < df_columns_per_table.iloc[k]['b']: # j = num cols in table k
        #print(df_columns.iloc[i + j][2])
        #columns_list_sum  = columns_list_sum + "sum(" + df_columns.iloc[j][2] + ") as sum_"+ df_columns.iloc[j][2] + ", "
        columns_list_min  = columns_list_min + "min(" + df_columns.iloc[i + j][2] + ") as min_"+ df_columns.iloc[i + j][2] + ", "
        columns_list_max  = columns_list_max + "max(" + df_columns.iloc[i + j][2] + ") as max_"+ df_columns.iloc[i + j][2] + ", "
        #columns_list_med  = columns_list_med + "median(" + df_columns.iloc[j][2] + ") as med_"+ df_columns.iloc[j][2] + ", "
        #columns_list_stdev  = columns_list_stdev + "stddev_pop(" + df_columns.iloc[j][2] + ") as stdev_"+ df_columns.iloc[j][2] + ", "
        columns_list_count  = columns_list_count + "count(" + df_columns.iloc[i + j][2] + ") as count_"+ df_columns.iloc[i + j][2] + ", "
        columns_list_countd  = columns_list_countd + "count(distinct " + df_columns.iloc[i + j][2] + ") as countd_"+ df_columns.iloc[i + j][2] + ", "
        j = j + 1
        #print("select builder end", k,j,i)
    
    #Now combine all select substrings into a select string
    select_fragment = (columns_list_min + columns_list_max +
                        columns_list_count + columns_list_countd)
    #reset the columns_list variables otherwise they keep growing!!
    columns_list_min = ""
    columns_list_max = ""
    columns_list_count = ""
    columns_list_countd = ""
    
    ## Now build my SQL query to reference each table
    my_sql_script = ("select " + 
    select_fragment[0:len(select_fragment)-2] #Remove space and comma at end of string (slicing)
    + 
    " from " + df_tables.iloc[k][0] + ";")
    k = k + 1
    #print(my_sql_script)
    #print (len(select_fragment)-1)
    #print (len(select_fragment))
    my_sql_script_list.append(my_sql_script)  #append sql statements into list
    #print("loop end before J reset", k,j,i)
    i = i + j #Accumulate the value of variable i which dictates the row index to find the start column name
    j = 0 #Reset J so that number of columns counter in table restarts
    print("loop end", k,j,i)
    

############# Fetch Data ###################
## Run the SQL scripts in my_sql_script_list and capture the results in a df

output_results = [] #initialise list
colname = [] #iniialise list
colnames = []
failed_queries = []


for my_sql in my_sql_script_list:
    try: # block raising an exception
        con= psycopg2.connect(dbname='dbname', host='host',port='port',user='user',password='password?')
        cur = con.cursor()
        print(my_sql[-(len(my_sql) - my_sql.rfind("from ") - 15):-1]) #print the table name of the query
        cur.execute(my_sql) ##e.g. select some data. Triple quotation for multi line strings.
        tmp = cur.fetchall() ## Fetch the results into an arbitrary data object
        output_results.append(tmp) ##Add data into my list of outputs
        colname = [desc[0] for desc in cur.description]
        colnames.append(colname)
        cur.close() ## Close cursor
        con.close() ## close connection
    except:
        #find function -- some column names have "from" in the title
        #must start finding from the right
        failed_queries.append(my_sql[-(len(my_sql) - my_sql.rfind("from ") - 15):-1]) #strip out table namess where the query fails
        #failed_queries.append()
        pass # doing nothing if an error is raised

#NOW TRAFORM THE RESULTS INTO THE FINAL DATA STRUCTURE REQUIRED
#Either Pandas DF or JSON Format
        
#Output output_results into a dataframe, and then join on colnames
#Output colnames list into dataframe via the same loop

print("All queries successfully completed")

output_results_df = pd.DataFrame() #initialise empty dataframe
output_results_df2 = pd.DataFrame() #initialise empty dataframe
colnames_df = pd.DataFrame() #initialise empty dataframe
colnames_df2 = pd.DataFrame() #initialise empty dataframe


m = 0 #arbitrary counter

for result in output_results:
    output_results_df = pd.DataFrame(output_results[m][0])  #mth list in parent list
    output_results_df2 = pd.concat([output_results_df2,output_results_df])
    #print(colnames[0])
    colnames_df = pd.DataFrame(colnames[m])
    colnames_df2 = pd.concat([colnames_df2,colnames_df])
    m = m + 1


#Finally, append the original table name for each column and value
#Repeat the table name for each new column name
#Get the final list of tables for which we have results
#Do this by removing any table names from the df_columns_in_table list that exist in the failed_queries list

for a in failed_queries:
    print(a)
    df_columns_per_table_completed = df_columns_per_table.drop(failed_queries)

y = 0
df_columns_per_table_completed_count = df_columns_per_table_completed.count()[0]

z = 0

final_table_name = [] #initialise empty list to insert repeating table names to append to cols and col values

for y in range(df_columns_per_table_completed_count):  #need to count over integer as cannot count over datframe
    #print(df_columns_per_table_completed.index.to_list()[y]) #table name
    tables_index = df_columns_per_table_completed.index #temporary index object of DF
    tables_list = tables_index.to_list() #Convert index object to a list
    #print(df_columns_per_table_completed.iloc[y]['b']) #number of columns
    number_of_cols = df_columns_per_table_completed.iloc[y]['b']
    
    for z in range(number_of_cols * 4): #each column has min,max,count and countd
        final_table_name.append(tables_list[y])
        z + 1 #Add to counter
    y + 1 #add to the counter



###########  final_output ##################
#join the table names and col details

#Join the two dataframes to append columns

colnames_df2.insert(1,"column_name",output_results_df2,True)


colnames_df2.insert(0,"table_name",final_table_name,True)



#Write output to CSV
colnames_df2.to_csv('G:\Shared drives\EcommPC\Customer Analytics\Python\DW Documentation_Lego.csv')



#import csv

#header = ["column_name", "metric_value"]

##Give the file some headers
#header = ['Type', 'OrderCurrency', 'OrderId', 'OrderTotal', 'CustomerEmail', 'OrderDate']

## Write to CSV. Write to Mention Me protected data files.
## datetime.today().strftime('%Y-%m-%d') -- altnerative with dashes included
#with open('G:\Shared drives\EcommPC\Customer Analytics\Python\DW Documentation '  '.csv', 'w', newline='') as csvFile: 
#    writer = csv.writer(csvFile)
#    writer.writerow(i for i in header)
#    writer.writerows(colnames_df2)
#csvFile.close() 
  


#    tmp = pd.DataFrame(tmp, columns=col_names)

# Print the dataset to the console
#print(dfs)

#Display the data types of each column
# NOTE: Data is currently stored as objects, rather than floats which affects the statistics returned from pandas.describe
# BUT, this is not critical. Continue as is for now
#print (dfs.dtypes)


