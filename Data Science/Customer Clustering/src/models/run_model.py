
#Initialise logging
import logging
log_fmt = '%(asctime)s - %(names)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format = log_fmt)


#import libraries reqruired
import pandas as pd
import numpy as np
import os
# Import preprocessing to normalize the data
from sklearn import preprocessing
# import KMeans
from sklearn.cluster import KMeans

#Import the data to a DF, convert to NP and normalise: 
def import_data():
    processed_data_file_path = os.path.join(os.path.pardir,'data','processed')
    processed_data_file_name = os.path.join(processed_data_file_path,'processed_customer_data.csv')
    df = pd.read_csv(processed_data_file_name,index_col = 'customer_code')
    #Convert df2 to np array for clustering
    #Since customer code is set to df index, this is excluded from the NP array which is what we want
    arr = df.to_numpy()
    #Normalise the data - this will keep the distributions within each feature but will give each feature equal weight
    arr_normalized = preprocessing.normalize(arr)
    return_data = [df,arr_normalized]
    return return_data

def cluster_data(return_data):
    #create kmeans object: Lets try with 7 clusters
    kmeans = KMeans(n_clusters=7)
    # fit kmeans object to data
    kmeans.fit(return_data[1])
    # Calculate the cluster index (labels) for the data points for clusters 1 to n
    y_km = kmeans.fit_predict(return_data[1])
    #Summarise Data in a pandas dataFrame
    out1 = pd.DataFrame(y_km,columns=['cluster_id'])
    #Join cluster_id back onto the main DF
    #Because indexes are different, add a column by extracting the values from the out1 df
    df_final = return_data[0]
    df_final['cluster_id'] = out1.loc[:'cluster_id'].values
    return df_final

#Output the data:
def export_data(df_final):
    #Filepaths for output of modelled data
    output_file_path = os.path.join(os.path.pardir,"data")
    output_file_name = os.path.join(output_file_path,"customer_clustering_results.csv")
    #output the data
    df_final.to_csv(output_file_name)

if __name__ == "__main__":
    return_data = import_data()
    df_final = cluster_data(return_data)
    export_data(df_final)