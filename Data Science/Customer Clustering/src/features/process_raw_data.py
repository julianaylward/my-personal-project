
#import packages
import os
import pandas as pd
import logging
import numpy as np

#Initialise logging
log_fmt = '%(asctime)s - %(names)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format = log_fmt)


#Raw data file location
def read_data():
    raw_data_file_path = os.path.join(os.path.pardir,'data','raw')
    raw_data_file_name = os.path.join(raw_data_file_path,'raw_customer_data.csv')
    #Read in raw data
    df = pd.read_csv(raw_data_file_name,index_col='customer_code')
    return df

def create_features(df):
    #using method chaining
    return (df
            .assign(bpo = lambda x: x.bottles / x.orders)
            .assign(abp = lambda x: np.where(x.bottles != 0, x.sales_ex_vat / x.bottles,0))
            .assign(pc_unique_skus = lambda x: np.where(x.bottles != 0,x.unique_skus / x.bottles,0))
            .assign(pc_orders_with_voucher = lambda x: x.orders_with_voucher / x.orders)
            .assign(pc_event_orders = lambda x: x.event_orders / x.orders)
           )


#Then remove outliers in the data:
#Remove extreme values which represent a very small % of the overall file
def clean_data(df):
    df = df[df.orders <=100]
    df = df[df.sales_ex_vat <= 30000]
    df = df[(df.bottles >= -200) & (df.bottles<=6000)]
    #And exclude ABP and BPO outliers:
    df = df[df.abp <= 100]
    df = df[df.bpo <=1000]
    return df


#Create a subset of the columns we want to cluster on:
#ABP, BPO and orders will cover sales ex vat and bottles (should co correlate) - we can check this
#And output processed data:

def write_to_file(df):
    columns = ['orders','abp','bpo','pc_unique_skus','pc_orders_with_voucher','pc_event_orders']
    #Write the processed file to CSV:
    processed_data_file_path = os.path.join(os.path.pardir,'data','processed')
    processed_data_file_name = os.path.join(processed_data_file_path,'processed_customer_data.csv')
    #Output desired colums to csv
    df[columns].to_csv(processed_data_file_name)

if __name__ == "__main__":
    df = read_data()
    df = create_features(df)
    df = clean_data(df)
    write_to_file(df)