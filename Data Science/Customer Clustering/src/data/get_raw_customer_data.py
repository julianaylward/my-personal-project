
#Import packages
import pandas as pd
import json
import psycopg2
import os
import logging
from dotenv import find_dotenv, load_dotenv

log_fmt = '%(asctime)s - %(names)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format = log_fmt)

#SQL to extract data from the DWH
sql = """select a.customer_code::numeric as uid,
round(count(distinct b.order_id) * random(),0) as orders,
round(count(distinct case when b.event_type = 'Event' then b.order_id else null end) * random(),0) as event_orders,
floor(sum(b.sale_price_ex_vat) * random()) as sales,
ceiling(sum(b.quantity) * random()) as quantity,
round(count(distinct b.product_code) * random()) as unique_skus,
floor((count(distinct c.order_id)::numeric(9,3) + 
	count(distinct case when d.voucher_code is not null then d.order_id else null end)) * random()) as orders_with_voucher	
from warehouse.d_customers_new a
inner join warehouse.d_order_products_new b
on a.customer_code = b.customer_code
inner join warehouse.d_orders_new d
on b.order_id = d.order_id
left join warehouse.d_vouchers_redeemed_new c
on b.order_id = c.order_id
where a.customer_type = 'Retail'
and b.product_group not in ('33')
and b.order_type <> 'Refund'
and left(lpad(a.customer_code,9,'0'),3) not in ('701','702','703','750')
and a.customer_status <> 'Lapsed'
and b.transaction_date > current_date - 720
group by a.customer_code
"""

#import connection details from connections json
def get_creds():
    dotenv_path = find_dotenv()
    load_dotenv(dotenv_path)
    servername = os.environ['REDSHIFT_DWH_ENDPOINT']
    database = os.environ['REDSHIFT_DWH_DATABASE']
    port = os.environ['REDSHIFT_DWH_PORT']
    username = os.environ['REDSHIFT_DWH_USERNAME']
    password = os.environ['REDSHIFT_DWH_PASSWORD']
    credentials = {'servername': servername, 'database': database, 'port':port, 'username':username,'password':password}
    return credentials #pass out creds as dict

#print (database,servername,port,username,password)

#print(sql)
def connect(credentials):
    con= psycopg2.connect(dbname= credentials['database'], host= credentials['servername'],
                        port= credentials['port'],user= credentials['username'],password= credentials['password'])
    cur = con.cursor()
    cur.execute(sql) ##e.g. select some data. Triple quotation for multi line strings.
    data_list = cur.fetchall() ## Fetch the results into a data frame

    # Extract the column names
    col_names = []
    for elt in cur.description:
        col_names.append(elt[0])
    cur.close() ## Close cursor
    con.close() ## close connection
    data_dict = {'data': data_list, 'col_names':col_names}
    return data_dict #pass out results as dict

#To pandas df
def to_df(data_dict):
    #Convert to pandas df
    df = pd.DataFrame(data_dict['data'],columns = data_dict['col_names'])
    #Swap index for customer_code
    df.set_index('uid')
    ## We're only interested in customers who have placed at least 3 orders, so strip them out
    df = df[df.orders >=3]
    return df

#Output the data
def output_to_csv(df):
    #Output the data to file
    raw_data_file_path = os.path.join(os.path.pardir,'data','raw')
    raw_data_file_name = os.path.join(raw_data_file_path,'raw_customer_data.csv')
    df.to_csv(raw_data_file_name, index=False)

if __name__ == "__main__":
    credentials = get_creds()
    data_dict = connect(credentials)
    df = to_df(data_dict)
    output_to_csv(df)