
#Download Kaggle data using the Kaggle API, Pluralsight method no longer seems to work
#https://technowhisp.com/kaggle-api-python-documentation/
#You need to first create an API key within your Kaggle account section and save in C:\users\username\.kaggle\kaggle.json

import logging

log_fmt = '%(asctime)s - %(names)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format = log_fmt)

from kaggle.api.kaggle_api_extended import KaggleApi
api = KaggleApi()
api.authenticate()

# Download single file for a competition
# Signature: competition_download_file(competition, file_name, path=None, force=False, quiet=False)
api.competition_download_file('titanic','train.csv', path = r'C:\Users\jaylward\titanic\data\raw')
api.competition_download_file('titanic','test.csv', path = r'C:\Users\jaylward\titanic\data\raw')

#kaggle.api.dataset_download_files('train.cvs', path='https://www.kaggle.com/c/titanic/download/', unzip=True)