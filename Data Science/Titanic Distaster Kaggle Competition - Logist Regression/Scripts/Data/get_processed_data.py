import pandas as pd
import numpy as np
import os

def read_data():
    #Set path of data:
    raw_data_path = os.path.join(os.path.pardir,'data','raw')
    train_file_path = os.path.join(raw_data_path,'train.csv')
    test_file_path = os.path.join(raw_data_path,'test.csv')
    #Read the data with default paramaters
    train_df = pd.read_csv(train_file_path, index_col = 'PassengerId')
    test_df = pd.read_csv(test_file_path, index_col = 'PassengerId')
    #Set default value for test dataset Survived
    test_df["Survived"] = -888
    df = pd.concat((train_df,test_df), axis = 0, sort = True)
    return df

# Process_data function pulls in other functions to transform the dataset
def process_data(df):
    #Using the method chaining concept:
    return (df
           #Create Title Attr and add this
            .assign(Title = lambda x : x.Name.map(GetTitle2))
            #Call an external function using .pipe
            .pipe(fill_missing_values)
            #Create fare_bin feature
            .assign(fare_bin = lambda x :pd.qcut(x.Fare,4,labels= ['low','medium','high','very high']))
            #create age_state feature
            .assign(age_state = lambda x: np.where(x['Age'] >= 18, 'Adult','Child'))
            #Family Size
            .assign(family_size = lambda x: x.Parch + x.SibSp + 1) #+1 includes ticket holder themselves)
            #is_mother
            .assign(is_mother = lambda x: np.where((x.Sex=='female') & (x.Age >=18) & (x.Parch >0) & (x.Title != 'miss'),1,0 ))
            #Create deck features
            .assign(Cabin = lambda x: np.where(x.Cabin=='T',np.NaN, x.Cabin))
            .assign(Deck = lambda x: x.Cabin.map(get_deck))
            #Feature encoding:
            .assign(is_male = lambda x : np.where(x.Sex =='male',1,0))
            .pipe(pd.get_dummies,columns=['Deck','Pclass','age_state','Title','fare_bin','Embarked']) #note diff here vs above
            .drop(['Cabin','Name','Ticket','Parch','SibSp','Sex'],axis = 1) #Axis 1 refers to columns)
            #reorder cols
            .pipe(reorder_columns)
           )


def GetTitle2(name):
    title_group = {
    'mr': 'mr',
    'mrs': 'mrs',
    'miss': 'miss',
    'master': 'master',
    'don': 'sir',
    'rev': 'sir',
    'dr': 'sir',
    'mme': 'mrs',
    'ms': 'mrs',
    'major': 'sir',
    'lady': 'lady',
    'sir': 'sir',
    'mlle': 'miss',
    'col': 'sir',
    'capt': 'sir',
    'the countess': 'lady',
    'jonkheer': 'sir',
    'dona': 'lady'
    }
    firstnamewithtitle = name.split(',')[1] #split passed variable using comma, access 2nd value
    title = firstnamewithtitle.split('.')[0] #first value from list spit by .
    title = title.strip().lower() #remove blanks and lowercase
    return title_group[title]


def fill_missing_values(df):
    #Fill null embarked
    df.Embarked.fillna('C',inplace=True)
    #Calcualte and fill null median fare
    median_fare = df.loc[(df.Pclass == 3) & (df.Embarked == 'S'),'Fare'].median()
    df.Fare.fillna(median_fare,inplace=True)
    #Age
    title_age_median = df.groupby('Title').Age.transform('median') 
    df.Age.fillna(title_age_median,inplace=True)
    return df


def get_deck(cabin):
    return np.where(pd.notnull(cabin),str(cabin)[0].upper(),'Z')
    
def reorder_columns(df):
    columns = [col for col in df.columns if col != 'Survived'] #col is a variable (e.g. for list_obj in list:)
    columns = ['Survived'] + columns
    df = df[columns]
    return df

def write_data(df):
    #Create file paths:
    processed_data_path = os.path.join(os.path.pardir,'data','processed')
    write_train_path = os.path.join(processed_data_path,'train.csv')
    write_test_path = os.path.join(processed_data_path,'test.csv')
    #wite data to file
    df.loc[df.Survived != -888].to_csv(write_train_path) #exclude test data where survived = default value
    columns = [col for col in df.columns if col != 'Survived'] #use list comprehension to exclude Survived from test data
    df.loc[df.Survived ==-888,columns].to_csv(write_test_path)


#Call the functions
if __name__ == '__main__':
    df = read_data()
    df = process_data(df)
    write_data(df)