
#Import modules:
from flask import Flask, request
import pandas as pd
import numpy as np
import pickle
import json
import os

app = Flask(__name__)

#Set model and scaler object file paths:
#we're 2 folders deep in /src/models, hence 2x os.path.pardir
model_path = os.path.join(os.path.pardir,os.path.pardir,'models')
model_filepath = os.path.join(model_path,'lr_models.pkl')
scaler_filepath = os.path.join(model_path,'lr_scalar.pkl')

#Load the model file and scaler files:
model = pickle.load(open(r'C:\Users\jaylward\titanic\models\lr_models.pkl','r+b'))
scaler = pickle.load(open(r'C:\Users\jaylward\titanic\models\lr_scalar.pkl','r+b'))


#columns we're going to pass to the api call:
#Needs to be unicode - by default this is the case in Python 3
columns = ['Age','Fare','family_size', \
          'is_mother','is_male','Deck_A','Deck_B','Deck_C','Deck_D', \
          'Deck_E','Deck_F','Deck_G','Deck_Z','Pclass_1','Pclass_2', \
          'Pclass_3', 'age_state_Adult', 'age_state_Child', 'Title_lady','Title_master','Title_miss','Title_mr', \
          'Title_mrs','Title_sir','fare_bin_low', \
          'fare_bin_medium','fare_bin_high', 'fare_bin_very high', 'Embarked_C', \
          'Embarked_Q', 'Embarked_S']

@app.route('/api', methods=['POST'])
def make_prediction():
    #read incoming json object and convert to string
    data = json.dumps(request.get_json(force=True))
    #Convert json string to pandas df
    df = pd.read_json(data)
    #Extract passenger IDs and convert to 1d np array:
    passenger_ids = df['PassengerId'].ravel()
    #Extract the actual values and convert to 1d np array:
    #Note: normally you wouldn't pass through actual, the point of the API is to predict!
    actuals = df['Survived'].ravel()
    #Extract the columns listed above and convert to matrix:
    X = df[columns].as_matrix().astype('float')
    #Transform the input to scale values using our unpickled scalar object
    X_scaled = scaler.transform(X)
    #Make predictions using unpickled model object:
    predictions = model.predict(X_scaled)
    #Create response dataframe
    #Note that we as passing through the ACTUAL result which normally you wouldn't have!
   #This is purely to demonstrate that the same result is achieved:
    df_response = pd.DataFrame({'PassengerId':passenger_ids,'Predicted':predictions,'Actual':actuals})
    #return as json
    return df_response.to_json()

    
if __name__ == "__main__":
    app.run(port=10001,debug=True)