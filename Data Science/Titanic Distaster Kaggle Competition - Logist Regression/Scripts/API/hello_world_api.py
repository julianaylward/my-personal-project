#Write script to disk

#Import flask
from flask import Flask, request

app = Flask(__name__)

@app.route('/api', methods = ['POST'])
def say_hello():
    data = request.get_json(force=True)
    name = data['name'] #extrac name attr from json file
    return "Hello {0}".format(name)


#app = Flask(__name__)
#@app.route("/")
#def hello():
#    return "Hello, World!"

if __name__ == '__main__':
    app.run(port=10001,debug=True)