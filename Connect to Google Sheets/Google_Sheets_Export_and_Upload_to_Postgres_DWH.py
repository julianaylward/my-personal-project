# -*- coding: utf-8 -*-
"""
Created on Mon May 18 15:33:30 2020

@author: jaylward
"""

from __future__ import print_function
import pickle
import os.path
import pandas as pd
#import datetime as dt
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.service_account import Credentials
from sqlalchemy import create_engine



# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1uJ2E9jrWvQ8GDAS7fppiYIK0Vubv0GOnFOEGHs8FbM4'
SAMPLE_RANGE_NAME = 'Sheet1!A:B'
KEY_FILE_LOCATION = 'connect-to-sheets-alteryx-api-742c8eb1ccc7.json'
creds = Credentials.from_service_account_file(KEY_FILE_LOCATION, scopes=SCOPES)
#conn = 'postgres+psycopg2://david.woodlock:eTskLPfJY-^6q*mX@dwh.mjwapps.com:5439/majestic'
conn = 'postgres+psycopg2://username:password@host:port/majestic'
sql_engine = create_engine(conn, echo=False)


def main_2():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets() # pylint: disable=no-member
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=SAMPLE_RANGE_NAME).execute()
    values = result.get('values', [])
    if not values:
        print('No data found.')
    else:
        print(values) ## Added by JA as test
        print('Name, Major:')
        for row in values:
            # Print columns A and E, which correspond to indices 0 and 4.
            print('%s, %s' % (row[0], row[4]))

#def run_connection(frame):
#    table='jca_master_voucher_data_1'
#    grant_privelage = "grant select on majestic.majestic_analyst_adh."+table+" to group business_user"
#    frame.to_sql(table, con=sql_engine, if_exists='replace', schema='majestic_analyst_adh', index=False,  method='multi')
#    sql_engine.execute(grant_privelage)
#    sql_engine.execute("COMMIT")
#    print("Privelage granted to table: "+table)

def main():
    service_call = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service_call.spreadsheets() # pylint: disable=no-member
    print("Calling Sheet")
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=SAMPLE_RANGE_NAME).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        print(values) #Works!!!
        values.pop(0)
        values.pop(0)
        df = pd.DataFrame.from_dict(data=values, orient='columns')
        df.replace({r'[^\x00-\x7F]|@|%|\'|{|}|\?|\#|\*+':'_'}, regex=True, inplace=True)
        df.replace(r'\n',  ' ', regex=True, inplace=True)
        #df.iloc[0].replace(r'\(|\)|\s', '_', regex=True, inplace=True)
        #print(df[16])
        #new_header = df.iloc[0] #grab the first row for the header
        #df = df[1:] #take the data less the header row
        df.columns = ['fruit','quantity']
        #'unique_id', 'owner', 'team', 'types', 'objective', 'product_id', 'product_description', 'promotion_id'
		#, 'promotion_description', 'discount_type', 'discount_value', 'mechanic', 'free_product_code', 'minimum_spend'
		#, 'minimum_count', 'region', 'stacking', 'requirement', 'detail', 'channel', 'partner', 'campaign_name', 'start_date'
		#, 'expiry_date', 'target_total_redemptions', 'target_new_redemptions', 'target_aov_inc_vat', 'discount_per_transaction'
		#, 'payment_type', 'total_media_spend', 'third_party_funding', 'reach', 'unit_cost_to_partner', 'cpa', 'anything_else'
		#, 'online_code', 'signed_off_by', 'reason_code', 'voucher_barcode'] #set the header row as the df header
        #print(df.loc[df['promotion_id'] == '3124'].loc[:, ['start_date', 'expiry_date']])
        #for (columnName, columnData) in df.iteritems():
        #    if 'Date'.lower() in str(columnName).lower():
        #        df.loc[:, (columnName)]=pd.to_datetime(df.loc[:, (columnName)], dayfirst=True).dt.strftime('%d/%m/%Y') #'%Y-%m-%d')
        #df.rename(columns = {list(df)[42:45]: ['CPA','Print','Category']}, inplace = True)format='%d/%m/%Y'
        #df['start_date'].replace({r'NaT':None}, regex=True, inplace=True)
        #df['expiry_date'].replace({r'NaT':'01/01/2099'}, regex=True, inplace=True)
        #print(df)
        #df.to_csv(r'./test.csv', index=False)
        #print("Establishing upload link")
        #run_connection(df)
        #print(df.loc[df['promotion_id'] == '3124'].loc[:, ['start_date', 'expiry_date']])
        print("Complete")
if __name__ == '__main__':
    main()
